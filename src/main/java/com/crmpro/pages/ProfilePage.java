package com.crmpro.pages;

import java.io.FileInputStream;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ProfilePage {

	WebDriver driver;
public ProfilePage(WebDriver ldriver)
    {
	this.driver=ldriver;
     }
@FindBy(xpath="//a[@href='mercuryregister.php']")
WebElement Profile_button;
@FindBy(xpath="//input[@name='firstName']")
WebElement First_Name;
@FindBy(xpath="//input[@name='lastName']")
WebElement Last_name;
@FindBy(xpath="//img[@src='images/profile_submit.gif']")
WebElement Submit_Button;
public void excelfileReading(String sheet1,String Sheet2,String Sheet3) throws BiffException, IOException, InterruptedException
{
String FilePath = "./TestData/Excel1.xls";
FileInputStream fs = new FileInputStream(FilePath);
Workbook wb = Workbook.getWorkbook(fs);
//String Sheetname="Login_Data";
Sheet sh = wb.getSheet(Sheet2);
int totalNoOfRows = sh.getRows();
int totalNoOfCols = sh.getColumns();
/*for (int row = 1; row < totalNoOfRows; row++)
{

for (int col = 0; col < totalNoOfCols; col++)
{
System.out.print(sh.getCell(col, row).getContents());
}
System.out.println();
       }
}*/
for (int row = 1; row < totalNoOfRows; row++)
{
First_Name.sendKeys(sh.getCell(0, row).getContents());
Thread.sleep(5000);
Last_name.sendKeys(sh.getCell(1, row).getContents());
Thread.sleep(3000);
Submit_Button.click();
//Thread.sleep(5000);
//String expectedtitle ="Find a Flight: Mercury Tours:";
//String actualtitle1=driver.getTitle();
//if (expectedtitle.equals(actualtitle1))
//{
//System.out.println("Passed");
//Submit_Button.click();
//Thread.sleep(5000);
//}
//else
//{
//System.out.println("Failed");
//Submit_Button.click();
//Thread.sleep(5000);
}
//System.out.println(sh.getCell(0, row).getContents());
//System.out.println(sh.getCell(1, row).getContents());
}
 public void Click_Profile_button(){
	 Profile_button.click(); 
 }
 }

