package com.crmpro.pages;

import java.io.FileInputStream;
import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.crmpro.utility.CommonMethod;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class LoginPage
{
 //abc
WebDriver driver;
 //abc
 
public LoginPage(WebDriver ldriver)
{
this.driver=ldriver;
}
@FindBy(xpath="//input[@name='userName']")
WebElement username;
@FindBy(xpath="//input[@name='password']")
WebElement Password;
@FindBy(xpath="//input[@name='login']")
WebElement Login_Button;
@FindBy(xpath="//a[@href='mercurywelcome.php']")
WebElement Home_button;
public void login_wordpress() throws IOException, InterruptedException
{
Thread.sleep(3000);
CommonMethod cm=new CommonMethod();
username.sendKeys(cm.property_file_reading("Username"));
Password.sendKeys(cm.property_file_reading("Password"));
Login_Button.click();


}
		
public boolean Verify_valid_login(){

	
String expectedtitle ="Find a Flight: Mercury Tours:";
String actualtitle1=driver.getTitle();
if (expectedtitle.equals(actualtitle1))
{
	return true;
}
else
{
	return false;
}

}
public void excelFileReading(String sheet1,String Sheet2,String Sheet3) throws BiffException, IOException, InterruptedException
{
String FilePath = "./TestData/Excel1.xls";
FileInputStream fs = new FileInputStream(FilePath);
Workbook wb = Workbook.getWorkbook(fs);
//String Sheetname="Login_Data";
Sheet sh = wb.getSheet(sheet1);
int totalNoOfRows = sh.getRows();
int totalNoOfCols = sh.getColumns();
/*for (int row = 1; row < totalNoOfRows; row++)
{

for (int col = 0; col < totalNoOfCols; col++)
{
System.out.print(sh.getCell(col, row).getContents());
}
System.out.println();
       }
}*/
for (int row = 1; row < totalNoOfRows; row++)
{
username.sendKeys(sh.getCell(0, row).getContents());
Thread.sleep(5000);
Password.sendKeys(sh.getCell(1, row).getContents());
Thread.sleep(3000);
Login_Button.click();
Thread.sleep(5000);
String expectedtitle ="Find a Flight: Mercury Tours:";
String actualtitle1=driver.getTitle();
if (expectedtitle.equals(actualtitle1))
{
System.out.println("Passed");
//Home_button.click();

ProfilePage Pp= new ProfilePage(driver);
Pp.Profile_button.click();
//Profile_button.click();
Thread.sleep(5000);
}
else
{
System.out.println("Failed");
Home_button.click();
Thread.sleep(5000);
}
//System.out.println(sh.getCell(0, row).getContents());
//System.out.println(sh.getCell(1, row).getContents());
}
       }

}

