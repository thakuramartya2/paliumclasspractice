package com.crmpro.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Browserfactory {

	static WebDriver driver;
	public static WebDriver StartBrowser(String Browsername,String url){
		
		if (Browsername.equalsIgnoreCase("firefox")){
			driver = new FirefoxDriver();
		}
		else if (Browsername.equalsIgnoreCase("chrome")){
			System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
			driver = new ChromeDriver();
			
		}
		else if (Browsername.equalsIgnoreCase("IE")){
			driver = new InternetExplorerDriver();
		}
	driver.manage().window().maximize();
	driver.get(url);
	return(driver);
	}
	}
