package com.crmpro.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.xml.xpath.XPath;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class CommonMethod {
	WebDriver driver;
	public String property_file_reading(String keyrequired) throws IOException
	{
		File file = new File("./TestData/Data.properties");
		FileInputStream fi = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fi);
		return prop.getProperty(keyrequired);
		
	}



//public void loginfromexcel() throws BiffException, IOException
//
//{
//String FilePath = "./TestData/ExcelPOI";
//FileInputStream fs = new FileInputStream(FilePath);
//Workbook wb = Workbook.getWorkbook(fs);
//String Sheetname="Sheet1";
//Sheet sh = wb.getSheet(Sheetname);
//int totalNoOfRows = sh.getRows();
//int totalNoOfCols = sh.getColumns();
//for (int row = 1; row < totalNoOfRows; row++) 
//	{
//
//	for (int col = 0; col < totalNoOfCols; col++)
//		{
//		String UserName = sh.getCell(0, row).getContents();
//		String PassWord = sh.getCell(1, col).getContents();
//		}
//       }
//}

public void Mouse_Event(WebElement xyz){
	
	
	Actions act=new Actions(driver);
	act.moveToElement(xyz).build().perform();
}
public void wait(XPath xyz){
	WebDriverWait wait1 = new WebDriverWait(driver,10);
    wait1.until(ExpectedConditions.visibilityOfElementLocated((By) xyz));
    	
    }
}

