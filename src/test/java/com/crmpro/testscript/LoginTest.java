package com.crmpro.testscript;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.crmpro.pages.LoginPage;
import com.crmpro.pages.ProfilePage;
import com.crmpro.utility.Browserfactory;
import com.crmpro.utility.CommonMethod;

import jxl.read.biff.BiffException;

public class LoginTest {
	WebDriver driver;
@BeforeMethod
public void BrowserAppLaunch() throws InterruptedException, IOException 
{
	CommonMethod cm=new CommonMethod();
	driver=Browserfactory.StartBrowser(cm.property_file_reading("Browser"), cm.property_file_reading("Mercuryurl"));
		
	Thread.sleep(5000);
}

@Test(enabled=true)
public void verifyvalidLogin() throws IOException, InterruptedException, BiffException{
	LoginPage login_page=PageFactory.initElements(driver, LoginPage.class);
	//login_page.login_wordpress();
	login_page.excelFileReading("Sheet1", "Sheet2", "Sheet3");
	ProfilePage Pp=new ProfilePage(driver);
	Pp.excelfileReading("", "Sheet2", "Sheet3");
}
//Assert.assertEquals(login_page.Verify_valid_login(), true);}	
@AfterMethod
public void BrowserClose()
{
	driver.quit();
}
}

